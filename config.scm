;; GNU guix configuration
;; ---
;; Configuration for Legacy GRUB booting starting a GNOME desktop environment.
;;
;; Based upon the operating system configuration template 'desktop.scm'
;; and 'lightweight-desktop.scm'

(use-modules (gnu) (gnu system nss))
(use-service-modules desktop xorg)
(use-package-modules admin aspell bash certs compression curl emacs file fonts fontutils gimp
                     gnome gnupg gnuzilla libreoffice messaging ncurses ssh version-control)

;; Custom packages
(define %custom-packages
  (list
   aspell
   aspell-dict-en
   bash
   curl
   emacs
   file
   font-dejavu
   font-fira-code
   font-fira-mono
   font-gnu-unifont
   font-liberation
   fontconfig
   git
   gimp
   gnome-tweaks
   gnupg
   gvfs ;; user mounts
   hexchat
   htop
   icecat
   icedove
   libreoffice
   ncurses
   nss-certs ;; certificates for secure HTTPS
   openssh
   pinentry-tty ;; gpg key entry
   unzip
   zip))

;; Modified services
(define %modified-services
  (modify-services
   %desktop-services
   (guix-service-type config =>
		              (guix-configuration
			           (inherit config)
			           (substitute-urls
			            (append (list "https://bordeaux.guix.gnu.org/")
				                %default-substitute-urls))
			           (authorized-keys
			            (append (list (local-file "./keys/bordeaux.guix.gnu.org.signing.key"))
				                %default-authorized-guix-keys))))))


(operating-system

 ;; Basic OS configuration
 (host-name "tara")
 (timezone "Europe/Berlin")
 (locale "en_US.utf8")
 (keyboard-layout (keyboard-layout "de"))

 ;; Bootloader
 (bootloader (bootloader-configuration
		      (bootloader grub-bootloader)
		      (targets '("/dev/sda"))
		      (keyboard-layout keyboard-layout)))

 ;; File systems
 (file-systems (cons (file-system
			          (device (uuid "1e6bd9bf-10b8-4faa-ac89-1ed3351e7df5"))
			          (mount-point "/")
			          (type "ext4"))
		             %base-file-systems))

 ;; Swap space
 (swap-devices (list (swap-space
			          (target (uuid "7797b31b-12f7-4ef7-ace1-2b184baeba2d")))))

 ;; User accounts and groups
 (users (cons (user-account
		       (name "piekarski")
		       (comment "Thomas Piekarski")
		       (home-directory "/home/piekarski")
		       (group "users")
		       (supplementary-groups '("audio" "cdrom" "netdev" "video" "wheel" )))
	          %base-user-accounts))

 ;; Additional packages next to %base-packages
 (packages (append %custom-packages
		           %base-packages))

 ;; GNOME and "desktop" services, which include the X11 log-in service,
 ;; simpler networking with NetworkManager, and a few more conveniences.
 (services (append (list (service gnome-desktop-service-type)
			             (set-xorg-configuration
			              (xorg-configuration
			               (keyboard-layout keyboard-layout))))
		           %modified-services))

 ;; Allow resolution of '.local' host names with mDNS.
 (name-service-switch %mdns-host-lookup-nss))
